import React from 'react';
import ReactDOM from 'react-dom';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import FileUploadProgress from 'react-fileupload-progress';
import ImagesUploader from 'react-images-uploader';
import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';
import * as actions from '../actions';
import Home from './Home';
import { API_URL } from '../config'



class MediaManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }



  render() {

    return (


      <div>
        <Home >
          <div>
            <div className="row">
              <div className="col-md-4">
                <h3>Media Management</h3>
              </div>
              <div className="col-md-8">
                <div className="button-continer text-right">
                  <a className="nav-link option-button">
                    <i className="icon-grid" />
                  </a>
                  <a className="nav-link option-button">
                    <i className="icon-list" />
                  </a>
                </div>
              </div>
            </div>
            <hr />
            <div className="row media">
              <div className="col-md-3">
                <h3>Select files</h3>
                <ImagesUploader
                  url="http://localhost:9090/multiple"
                  optimisticPreviews
                  onLoadEnd={(err) => {
                    if (err) {
                      console.error(err);
                    }
                  }}
                  label="Upload multiple images"
                />
              </div>
             
            </div
            ></div>

        </Home>
      </div>


    )
  }
};

const mapStateToProps = state => ({
  token: state.admin.user.access_token
});

export default withRouter(connect(mapStateToProps, actions)(MediaManagement))
